#!/bin/python
"""
This script generates a webpage to display the results of a BayesWave run.

This script was originally put together by Jonah Kanner and Francecso Pannarale.
Major and minor modifications have been made by Meg Millhouse, Sudarshan Ghonge, and probably others.

This code is tested with python2.7
"""

######################################################################################################################
#
# Import modules
#
######################################################################################################################
import argparse
import glob
import math
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import scipy.stats as scs
import numpy as np
import os
import pwd
import subprocess
import sys
import re
import traceback

print('Path to megaplot: ')
print(sys.argv[0])
print('\n')


# Allow navigation into specified working directory
topdir=os.getcwd()
try:
    workdir=sys.argv[1]
except IndexError:
    # No work directory specified, workdir=./
    workdir=os.getcwd()
os.chdir(workdir)

import pkg_resources
postprocesspath = pkg_resources.resource_filename('bayeswave_plot',
    '../bayeswave_plot_data/')

from scipy import integrate

######################################################################################################################
#
# Dictionaries and other global variables
#
######################################################################################################################
# Item labels must coincide with moment names given in BayesWavePost
# 1st entry = x-axis label
# 2nd entry = file name tag
moments_dict = {}
moments_dict['dur_rec']               = ['Duration', 'duration']
moments_dict['t_energy_rec']          = ['log10(Signal Energy)', 'energy']
moments_dict['f0_rec']                = ['Central Freq (Hz)', 'freq']
moments_dict['band_rec']              = ['Bandwidth (Hz)', 'band']
moments_dict['t0_rec']                = ['Central Time (s)', 'tzero']
moments_dict['overlap']               = ['Overlap - recovered signal and injection', 'overlap']
moments_dict['network_overlap']       = ['Network overlap - recovered signal and injection', 'network_overlap']
moments_dict['snr']                   = ['Recovered SNR','snr']

# 1st entry = html file name
# 2nd entry = subpage header
# 3rd entry = plot file name tag
html_dict = {}
html_dict['dur_rec']           = ['duration', 'Histogram of duration', moments_dict['dur_rec'][1]]
html_dict['t_energy_rec']      = ['energy', 'Histogram of signal energy, (aka ||<i>h(t)</i>||<sup>2</sup>)', moments_dict['t_energy_rec'][1]]
html_dict['f0_rec']            = ['f0', 'Histogram of central frequency', moments_dict['f0_rec'][1]]
html_dict['band_rec']          = ['band', 'Histogram of bandwidth', moments_dict['band_rec'][1]]
html_dict['t0_rec']            = ['t0', 'Histogram of central time', moments_dict['t0_rec'][1]]
html_dict['overlap']           = ['overlap', 'Overlap Histogram between recovered signal and injection', moments_dict['overlap'][1]]
html_dict['signal_waveform']   = ['', 'Median Signal Model Waveforms and 1-sigma Uncertainties', 'signal_waveform']
html_dict['glitch_waveform']   = ['', 'Median Glitch Model Waveforms and 1-sigma Uncertainties', 'glitch_waveform']
html_dict['spec']              = ['spec', 'Spectrogram of median reconstructed model waveform', 'self_spec']
html_dict['diagnostics']       = ['diagnostics', 'Diagnostic plots']
html_dict['snr']               = ['snr','SNR','snr']

modelList = ('signal', 'glitch')

postDir   = 'post/'
plotsDir  = 'plots/'
htmlDir   = 'html/'

#### --fullOnly ###
fullOnly_flag = 0
if os.path.exists(postDir+'full'):
    fullOnly_flag = 1
    print("Found --fullOnly flag\n")


#Adopt common color scheme for different models
ncolor = 'darkgrey'
gcolor = 'darkgoldenrod'
scolor = 'darkorchid'

ifoColors = ['darkgoldenrod','darkkhaki','darkseagreen','olive','cadetblue','green','slategray','darkcyan']
signal_ifoColorList = ['darkorchid','fuchsia','indigo','orchid','slateblue','mediumvioletred','palevioletred']

injcolor = 'teal'

######################################################################################################################
#
# Read in run data and info
#
######################################################################################################################

# ----------------------------------
# Extract information about the run
# ----------------------------------
def readbwb():
    # -- Initialize variables with info about the job
    info = ""
    ifoNames = []
    ifoList = []
    jobName = ''
    restrictModel = ''
    injFlag = False
    lalsimFlag = False
    mdc = False
    snrList = []
    # -- Open *bayeswave.run 
    bayeswaverunfile = glob.glob('*bayeswave.run')
    if not len(bayeswaverunfile):
        sys.exit("\n *bayeswave.run not found! \n")
    bayeswaverunfile = bayeswaverunfile[0]
    bayeswave = open(bayeswaverunfile, 'r')
    # -- Parse command line arguments
    raw_lines = bayeswave.readlines()
    lines = [l.lstrip() for l in raw_lines]
    cmdline = ''
    for l in lines:
        if 'Command' in l:
            cmdline = l.rstrip().split(' ')

    for index, arg in enumerate(cmdline):
        # Determine the IFOs involved
        if arg=='--ifo':
            ifoNames.append(cmdline[index+1])
        # Determine the trigger time
        elif arg=='--trigtime':
            gps = float(cmdline[index+1])
            info = info + "The trigger time is GPS: {0}\n".format(gps)
        # Determine the job name
        elif arg=='--runName':
            jobName = cmdline[index+1]
            print("The job name is: {0}".format(jobName))
            jobName = jobName+'_'
        # Determine if the job was glitchOnly, noiseOnly, or signalOnly
        elif arg=='--glitchOnly':
            print('\nThis run was executed with the --glitchOnly flag\n')
            restrictModel = 'glitch'
        elif arg=='--noiseOnly':
            print('\nThis run was executed with the --noiseOnly flag\n')
            restrictModel = 'noise'
        elif arg=='--signalOnly':
            print('\nThis run was executed with the --signalOnly flag\n')
            restrictModel = 'signal'
        elif arg=='--inj':
            injFlag = True
            mdc = True
        elif arg=='--MDC-cache':
            mdc = True
        elif arg=='--lalinspiralinjection':
            lalsimFlag = True

    if lalsimFlag: injFlag = False
    # -- Determine number of IFOs
    ifoNum = len(ifoNames)
    # -- Convert the IFO name list into a list of numbers useful for opening datafiles
    ifoList = [str(ii) for ii in range(0,len(ifoNames))]
    info = info + "Detectors(s) found: {0}\n".format(len(ifoList))
    info = info + "The detectors are: {0}\n".format(', '.join(ifoNames))
    # -- If MDC, read SNR info
    if mdc:
        for ifo in ifoList:
            snrFile = jobName+'post/injection_whitened_moments_%s.dat'%(ifoNames[int(ifo)])
            if not os.path.exists(snrFile):
                sys.exit("\n {0} not found! \n".format(snrFile))
            snrdata = open(snrFile, 'r')
            snrdata.readline()
            snr = float(snrdata.readline().split(' ')[0])
            snrList.append(snr)
            snrdata.close()
            info = info + 'Injected SNR in detector {0} = {1}\n'.format(ifoNames[int(ifo)],snrList[-1])
    bayeswave.close()
    # -- Report to user
    print("{0}".format(info))
    return(jobName, restrictModel, mdc, injFlag, bayeswaverunfile, ifoList, ifoNames, gps, snrList, info)


# ---------------------------------------------
# Get Median waveform with CIs
# ---------------------------------------------
def get_waveform(filename):
    names = ['samples','median_waveform','50low','50high','90low','90high']
    data = np.recfromtxt(filename,names=names)
    return (data['samples'],data['median_waveform'],data['50low'],data['50high'],data['90low'],data['90high'])

    

######################################################################################################################
#
# Plotting functions
#
######################################################################################################################
#....................................................
# New function to get SNR^2(t) for x-axis
#....................................................
def snrfunctime(median_waveform):
    powerList = []
    wave_integral = median_waveform
    time_integrate = time
    #integral at each
    dt = 0.0009765
    h_t_2 = []
    for line, line_1, time_i in zip(wave_integral, wave_integral[1:], time_integrate):
        t = time_i
        w0 = line
        h_t_2.append(line**2)
        w1  = line_1
        snr_t = (((w0**2)+(w1**2))/2)*dt
        powerList.append([snr_t,t])

    return(powerList)

# -------------------------------------------------
# Read in data and median waveform to get plot axis
# Now using SNR^2 for time axis determination
# -------------------------------------------------
def get_axes(jobName, postDir, ifoList, model, time, fullOnly_flag=0):
    axisList = []
    for ifo in ifoList:
        # -- Read Signal model
        if fullOnly_flag==1:
            filename = str(jobName)+postDir+'full/{1}_median_time_domain_waveform_{0}.dat'.format(ifoNames[int(ifo)], model)
        else:
            filename = str(jobName)+postDir+'{1}/{1}_median_time_domain_waveform_{0}.dat'.format(ifoNames[int(ifo)], model)
        try:
            timesamp, median_waveform, dummy1, dummy2, down_vec, up_vec = get_waveform(filename)
        except:
            print("Couldn't find waveform file %s" % filename)


        # -- Get axis info
        snr_t = snrfunctime(median_waveform)
        #y
        wave = up_vec
        wave_max = wave.max()
        #x
        wave = median_waveform
        power, and_time = zip(*snr_t)
        power = list(power)
        sig_times = []
        #Fixed ~90$ rep. from 5% to 95%
        for row in snr_t:
            if (row[0] >= 0.0001*np.max(power) and row[0] <= 0.9999*np.max(power)):
                sig_times.append(row[1])
        axisList.append([np.min(sig_times), np.max(sig_times)+0.1*(np.max(sig_times)-np.min(sig_times)), -wave_max*1.1, wave_max*1.1])
    
    
    # -- Select Axis with biggest y-value
    ymax = 0
    for axcand in axisList:
        if axcand[3] > ymax:
            ymax = axcand[3]
            axwinner = axcand
    return(axwinner)


def get_axes_fdom(jobName, postDir, ifoList, model, time):
    
    ymin = np.zeros(len(ifoList))
    ymax = np.zeros(len(ifoList))
    
    for ifo in ifoList:
        # -- Read Signal model
    
        names = ['f','dpower','rpower','psd']
    
        filename = str(jobName)+postDir+'gaussian_noise_model_{0}.dat'.format(ifoNames[int(ifo)])
        
        data = np.recfromtxt(filename,names=names)
        
        # figure out what fmin is-- psd padded with 1's until fmin
        imin = 0
        while data['psd'][imin] == 1:
            imin += 1
        
        ymin[int(ifo)] = min(data['rpower'][imin:])
        ymax[int(ifo)] = max(data['rpower'][imin:])

        xmin = data['f'][imin]
        xmax = data['f'][-1]
    

    axwinner = [xmin, xmax, min(ymin), max(ymax)]
    return(axwinner)


# --------------
# Plot Evidence
# --------------
def plot_evidence(jobName, plotsDir):

    sig_noise=0
    sig_gl=0
    sig_si=0
    err_sig_noise=0
    err_sig_gl=0
    err_sig_si=0

    # -- Read evidence data
    try:
        infile = open(str(jobName)+'evidence.dat', 'r')
        maxodds = 20
        for line in infile:
            spl = line.split()
            if spl[0] == 'noise':
                sig_noise = float(spl[1])
                err_sig_noise = float(spl[2])
                #evidence.dat files have alread store variance (not std dev)
                #err_sig_noise *= err_sig_noise
            if spl[0] == 'glitch':
                sig_gl = float(spl[1])
                err_sig_gl = float(spl[2])
                #err_sig_gl *= err_sig_gl
            if spl[0] == 'signal':
                sig_si = float(spl[1])
                err_sig_si = float(spl[2])
                #err_sig_si *= err_sig_si
        infile.close()
    except:
        sig_noise=0
        sig_gl=0
        sig_si=0
        err_sig_noise=0
        err_sig_gl=0
        err_sig_si=0

    sig_noise = sig_si - sig_noise
    sig_gl    = sig_si - sig_gl

    err_sig_noise += err_sig_si
    err_sig_noise = math.sqrt(err_sig_noise)
    err_sig_gl += err_sig_si
    err_sig_gl = math.sqrt(err_sig_gl)
    # -- Report to the user 
    print('   log( E_signal / E_noise ) =', sig_noise)
    print('   log( E_signal / E_glitch ) =', sig_gl)
    # -- Plot the data point 
    plt.figure()
    plt.errorbar(sig_gl, sig_noise, 2*err_sig_gl, 2*err_sig_noise, color='black')
    # -- Store maxima and establish axes 
    maxodds = 1.1*np.array( [np.abs(sig_gl)+2*err_sig_gl, np.abs(sig_noise)+2*err_sig_noise, 20] ).max()
    xmaxodds = 1.1*np.maximum(np.abs(sig_gl)+2*err_sig_gl, 20)
    ymaxodds = 1.1*np.maximum(np.abs(sig_noise)+2*err_sig_noise, 20)
    plt.axis([-xmaxodds, xmaxodds, -ymaxodds, ymaxodds])
    # -- Color in the plot 
    plt.fill_between([0,maxodds], [0, 0], [maxodds, maxodds], facecolor=scolor, interpolate=True, alpha=0.3)
    plt.fill_between([-maxodds,0,maxodds], [-maxodds,0,0], [-maxodds, -maxodds, -maxodds], facecolor=ncolor, interpolate=True, alpha=0.3)
    plt.fill_between([-maxodds,0], [-maxodds, 0], [maxodds, maxodds], facecolor=gcolor, interpolate=True, alpha=0.3)
    plt.grid()
    # -- Labels on the plot
    plt.text(0.9*xmaxodds, 0.9*ymaxodds, 'Signal', horizontalalignment='right', verticalalignment='top')
    plt.text(-0.9*xmaxodds, 0.9*ymaxodds, 'Glitch', horizontalalignment='left', verticalalignment='top')
    plt.text(0.9*xmaxodds, -0.9*ymaxodds, 'Noise', horizontalalignment='right', verticalalignment='bottom')
    # -- Final touches 
    plt.xlabel(r'LN($B_{signal/glitch}$)')
    plt.ylabel(r'LN($B_{signal/noise}$)')
    plt.savefig(plotsDir+'odds.png')
    plt.close()
    
    
    # -- Return values to be used later
    return(sig_gl, sig_noise, err_sig_gl, err_sig_noise)

# -----------------------------------------------
# Plot the median waveform and injected waveform
# -----------------------------------------------
def plot_waveform(jobName, postDir, ifo, plotsDir, worc, mdc, model, axwinner, time, low_50, high_50, low_90, high_90, median_waveform, second_low_50=[], second_high_50=[], second_low_90=[], second_high_90=[], second_median_waveform=[]):
    
    plt.figure()

    # This gets the *data*
    try:
        filename = str(jobName)+postDir+'{1}_data_{0}.dat'.format(ifoNames[int(ifo)], worc)
        ifo_data = np.genfromtxt(filename)
        plt.plot(time, ifo_data, color = '0.75', linewidth=2, alpha=0.25)
    except:
        print("I couldn't find the file {0}".format(filename))

    if model == 'glitch':
       colour = gcolor
    elif model == 'signal':
       colour = scolor
    elif model == 'full':
        colour = scolor

    plt.fill_between(time, low_50, high_50, facecolor=colour, edgecolor=colour, alpha=0.5)
    plt.fill_between(time, low_90, high_90, facecolor=colour, edgecolor=colour, alpha=0.3)

    # full model double plotting
    if model == 'full':
      plt.fill_between(time, second_low_50, second_high_50, facecolor=gcolor, edgecolor=gcolor, alpha=0.5)
      plt.fill_between(time, second_low_90, second_high_90, facecolor=gcolor, edgecolor=gcolor, alpha=0.3)

    if mdc:
        # -- Read in the injected waveform
        try:
            filename = str(jobName)+postDir+'injected_{1}_waveform_{0}.dat'.format(ifoNames[int(ifo)], worc)
            inj_median_waveform = np.genfromtxt(filename)
        except:
            filename = str(jobName)+postDir+'injected_waveform_{0}.dat'.format(ifoNames[int(ifo)])
            inj_median_waveform = np.genfromtxt(filename)
        inj_time = time
        plt.plot(inj_time, inj_median_waveform, injcolor, linewidth=1)

    plt.plot(time, median_waveform, color=colour, linewidth=1, alpha=1)
    if model == 'full':
        plt.plot(time, second_median_waveform, color=gcolor, linewidth=1, alpha=1)

    #axisList.append([sig_times.min(), sig_times.max(), -wave_max*1.1, wave_max*1.1])
    plt.xlabel('Time (s)')
    plt.ylabel('Whitened strain'.format(model,colour))
    plt.title('Reconstructed {0} model in {1}'.format(model,ifoNames[int(ifo)]))

    # -- Save the full versions of the plot
    if worc == 'whitened':
        plt.savefig(plotsDir+'{1}_waveform_{0}_full.png'.format(ifoNames[int(ifo)], model))
    else:
        plt.savefig(plotsDir+'c{1}_waveform_{0}_full.png'.format(ifoNames[int(ifo)], model))

    plt.axis(axwinner)
    # -- Save the plot
    if worc == 'whitened':
        plt.savefig(plotsDir+'{1}_waveform_{0}.png'.format(ifoNames[int(ifo)], model))
    else:
        plt.savefig(plotsDir+'c{1}_waveform_{0}.png'.format(ifoNames[int(ifo)], model))

    plt.close()




def plot_power_spectrum(jobName, postDir, ifo, plotsDir, worc, mdc, model, axwinner, freq, low_50, high_50, low_90, high_90, median_waveform,type):
    plt.figure()
    
    # To do: data and injected waveforms if possible
    
    if model == 'glitch':
        colour = gcolor
    elif model == 'signal':
        colour = scolor


    if mdc:
        filename = st(jobName)+postDir+'injected_whitened_spectrum_{0}.dat'.format(ifoNames[int(ifo)])

        injected_spectrum = np.genfromtxt(filename)

        plt.semilogy(injected_spectrum[:,0],injected_spectrum[:,1],color = '0.75', linewidth=2, alpha=0.25)



    plt.fill_between(freq, low_50, high_50, facecolor=colour, edgecolor=colour, alpha=0.5)
    plt.fill_between(freq, low_90, high_90, facecolor=colour, edgecolor=colour, alpha=0.3)
    
    if mdc:
        # -- Read in the injected waveform
        try:
            filename = str(jobName)+postDir+'injected_{1}_waveform_{0}.dat'.format(ifoNames[int(ifo)], worc)
            inj_median_waveform = np.genfromtxt(filename)
        except:
            filename = str(jobName)+postDir+'injected_waveform_{0}.dat'.format(ifoNames[int(ifo)])
            inj_median_waveform = np.genfromtxt(filename)
        inj_time = time
        plt.plot(inj_time, inj_median_waveform, injcolor, linewidth=1)

    if type == 'psd':
        filename = str(jobName)+postDir+'gaussian_noise_model_{0}.dat'.format(ifoNames[int(ifo)])
        data = np.recfromtxt(filename, names = ['f', 'dpower', 'rpower', 'Sn'])
        plt.semilogy(data['f'],data['rpower'], ncolor, alpha=0.6)
        plt.ylim(min(data['rpower']),max(data['rpower']))


    plt.semilogy(freq, median_waveform, color=colour, linewidth=1, alpha=1)
    
    #axisList.append([sig_times.min(), sig_times.max(), -wave_max*1.1, wave_max*1.1])
    plt.xlabel('Frequency (Hz)')
    if type == 'psd':
        plt.ylabel('PSD and data (grey)'.format(model))
    if type == 'powerspec':
         plt.ylabel('Whitened power spectrum of median waveform'.format(model))
    plt.title('{0}'.format(ifoNames[int(ifo)]))



    # -- Save the full versions of the plot
    if worc == 'whitened':
        plt.savefig(plotsDir+'{1}_{2}_{0}_full.png'.format(ifoNames[int(ifo)], model, type))
    else:
        plt.savefig(plotsDir+'c{1}_{2}_{0}_full.png'.format(ifoNames[int(ifo)], model, type))


    plt.xlim(32,512)
    
#plt.axis(axwinner)
    # -- Save the plot
    if worc == 'whitened':
        plt.savefig(plotsDir+'{1}_{2}_{0}.png'.format(ifoNames[int(ifo)], model, type))
    else:
        plt.savefig(plotsDir+'c{1}_{2}_{0}.png'.format(ifoNames[int(ifo)], model, type))
    
    plt.close()

def plot_full_spectro(jobName, postDir, ifo, plotsDir, worc, mdc, model, axwinner, psd_info, powerspec_info, median_waveform):
    
    plt.figure()

    if model == 'glitch':
        colour = gcolor
    elif model == 'signal':
        colour = scolor


    if mdc:
        filename = str(jobName)+postDir+'injected_whitened_spectrum_{0}.dat'.format(ifoNames[int(ifo)])

        injected_spectrum = np.genfromtxt(filename)

        plt.semilogy(injected_spectrum[:,0],injected_spectrum[:,1],injcolor, linewidth=1)


    names = ['f','dpower','rpower','psd']
    filename = str(jobName)+postDir+'gaussian_noise_model_{0}.dat'.format(ifoNames[int(ifo)])
    data = np.recfromtxt(filename,names=names)

    # plot data
    plt.semilogx(data['f'],data['rpower'],color=ncolor,alpha=0.5)

    # plot psd
    plt.fill_between(psd_info[0],psd_info[4],psd_info[5],color='grey',alpha=0.8)
    plt.semilogy(psd_info[0],psd_info[1],color='k',ls='-')

    # plot powerspec
    plt.fill_between(powerspec_info[0],powerspec_info[2],powerspec_info[3],color=colour,alpha=0.5)
    plt.fill_between(powerspec_info[0],powerspec_info[4],powerspec_info[5],color=colour,alpha=0.3)
    plt.plot(powerspec_info[0],powerspec_info[1],color=colour)

    plt.xscale('log')
    plt.yscale('log',nonposy='clip')

    plt.ylim(axwinner[2],axwinner[3])
    plt.xlim(axwinner[0],axwinner[1])

    plt.xlabel("Frequency (Hz)")
    plt.ylabel("Power")
    plt.title("Power Spectra for {0} model in {1}".format(model,ifoNames[int(ifo)]))


    plt.savefig(plotsDir+'{0}_frequence_domain_{1}.png'.format(model,ifoNames[int(ifo)]))

    plt.close()



def plot_tf_tracks(jobName, postDir, ifo, plotsDir, worc, mdc, model, axwinner, f_axwinner,time, low_50, high_50, low_90, high_90, median_waveform):
    plt.figure()
    
    # To do: data and injected waveforms if possible
    
    if model == 'glitch':
        colour = gcolor
    elif model == 'signal':
        colour = scolor

    #t = time - 2.0
    t = time;

    # -- Kludgy fix to keep megaplot from crashing when data is bad
    if np.amax(high_50) > 1e5: # Something is wrong
      high_50 = np.zeros(high_50.shape)

    if np.amax(high_90) > 1e5:
      high_90 = np.zeros(high_90.shape)


    plt.fill_between(t, low_50, high_50, facecolor=colour, edgecolor=colour, alpha=0.5)
    plt.fill_between(t, low_90, high_90, facecolor=colour, edgecolor=colour, alpha=0.3)

    if mdc:
        # -- Read in the injected waveform
        try:
            filename = str(jobName)+postDir+'injected_tf_{0}.dat'.format(ifoNames[int(ifo)], worc)
            inj_median_waveform = np.genfromtxt(filename)
        except:
            filename = str(jobName)+postDir+'injected_tf_{0}.dat'.format(ifoNames[int(ifo)])
            inj_median_waveform = np.genfromtxt(filename)
        inj_time = time
        plt.plot(inj_time, inj_median_waveform, injcolor, linewidth=1)
    
    
    plt.plot(t, median_waveform, color=colour, linewidth=1, alpha=1)
    
    plt.xlabel('Time (s)')
    plt.ylabel('Frequency (Hz)')
    plt.title('Time vs. Frequency for {0} model in {1}'.format(model,ifoNames[int(ifo)]))
    
    
    
    # -- Save the full versions of the plot
    plt.savefig(plotsDir+'{1}_tf_{0}_full.png'.format(ifoNames[int(ifo)], model))


    plt.xlim(axwinner[0],axwinner[1])
    plt.ylim(f_axwinner[0],f_axwinner[1])
    
    #plt.axis(axwinner)
    # -- Save the plot
    plt.savefig(plotsDir+'{1}_tf_{0}.png'.format(ifoNames[int(ifo)], model))
    
    plt.close()


# --------------------------------------
# Plot Q scans of waveforms
# --------------------------------------
def Q_scan(subDir,model, Q, t, f, ifo, axwinner, f_axwinner, climwinner=[1,1], fullOnly_flag=0):
    if fullOnly_flag==1:
        filename = postDir+'/full/{0}_spectrogram_{1}_{2}.dat'.format(model, Q, ifoNames[int(ifo)])
    else:
        filename = postDir+subDir+'/'+'{0}_spectrogram_{1}_{2}.dat'.format(model, Q, ifoNames[int(ifo)])

    data = np.genfromtxt(filename)

    fig = plt.figure()
    plt.imshow(data,aspect='auto',origin='lower',extent=[t[0],t[-1],f[0],f[-1]],cmap='OrRd')

    if model == 'data':
        plt.clim(np.min(data),np.max(data))

    if model == 'signal_residual' or model == 'glitch_residual':
        plt.clim(climwinner)

    plt.xlabel('Time (s)')
    plt.ylabel('Frequency (Hz)')
    plt.title('Spectrogram of median {0} waveform in {1}, Q={2}'.format(model.replace('_','\_'),ifoNames[int(ifo)], Q))

    plt.xlim(axwinner[0],axwinner[1])
    plt.ylim(f_axwinner[0],f_axwinner[1])

    plt.savefig(plotsDir+'{0}_spectrogram_Q{1}_{2}.png'.format(model,Q,ifoNames[int(ifo)]))

    plt.close()

    return [np.min(data),np.max(data)]


# -----------
# Histograms
# -----------
def make_histogram(moments, moment, model, ifo, plotsDir):
    if model == "signal":
        colour = scolor
    if model == "glitch":
        colour = gcolor
    injcolour = injcolor

    # -- Start figure
    plt.figure()

    histmoments = moments[moment]

    if moment == 't0_rec': # Center recovered time at 0s
        rn,rbins,patches = plt.hist(histmoments - 2, bins=50, label='Recovered', alpha=0.5, linewidth=0, color=colour)
    elif 'overlap' in moment: # For overlap want to make sure we get the right axes range
        rbins = np.arange(-1.02,1.02,0.01)
        rn,rbins,patches = plt.hist(histmoments, rbins, alpha=0.5, linewidth=0, color=colour)
        plt.xlim(-1.1, 1.1)
    else:
        rn,rbins,patches = plt.hist(histmoments, bins=50, label='Recovered', alpha=0.5, linewidth=0, color=colour)

    if 'overlap' not in moment:
        if moment == 't0_rec':
            inmode = np.median(injmoments[moment] - 2) # Correcting for the 2s added by BW to the data time window
        else:
            inmode = np.median(injmoments[moment])

        if mdc:
            plt.axvline(x=inmode, label='Injected', color=injcolour)
            plt.legend()

    # -- Set title by IFO, or overlap ()
    if 'network_overlap' in moment:
        plt.title('Network Overlap')
    else:
        plt.title('{0}'.format(ifoNames[int(ifo)]))

    # -- Plot settings
    plt.ylim(1, 2*rn.max())
    plt.xlabel(moments_dict[moment][0])
    plt.grid()
        
    if not moment == 'network_overlap':
        plt.savefig(plotsDir+model+'_'+moments_dict[moment][1]+'_{0}.png'.format(ifoNames[int(ifo)]))
    else:
        plt.savefig(plotsDir+model+'_'+moments_dict[moment][1]+'.png')
                    
    plt.close()



# -----------------
# Plot Temp Chains
# -----------------
def plot_likelihood_1(modelList, plotsDir, fullOnly_flag=0):
    plt.figure()
    plt.title('Likelihood')
    plt.xlim([1e-4, 1])
    minlist = []
    maxlist = []

    if fullOnly_flag:
        mod = 'full'
        colour = 'seagreen'
        try:
            names = ['temp','likehood','error','acl']
            data = np.recfromtxt(str(jobName)+"{0}_evidence.dat".format(mod), names=names)
        except:
            try:
                names = ['temp','likehood','error']
                data = np.recfromtxt(str(jobName)+"{0}_evidence.dat".format(mod), names=names)
            except:
                pass
        #error = np.zeros(likehood.shape)
        plt.semilogx(data['temp'], data['likehood'], label=mod, linewidth=2, color=colour)
        plt.errorbar(data['temp'], data['likehood'], 2*data['error'], color=colour)
        minlist.append(data['likehood'][np.where(data['temp']>1e-4)[0][-1]])
        maxlist.append(data['likehood'][0])



    else:
        for mod in modelList:
            if mod == 'glitch':
                colour = gcolor
            elif mod == 'signal':
                colour = scolor
            else:
                colour = ncolor
            try:
                names = ['temp','likehood','error','acl']
                data = np.recfromtxt(str(jobName)+"{0}_evidence.dat".format(mod), names=names)
            except:
                try:
                   names = ['temp','likehood','error']
                   data = np.recfromtxt(str(jobName)+"{0}_evidence.dat".format(mod), names=names)
                except:
                    continue
            #error = np.zeros(likehood.shape)
            plt.semilogx(data['temp'], data['likehood'], label=mod, linewidth=2, color=colour)
            plt.errorbar(data['temp'], data['likehood'], 2*data['error'], color=colour)
            minlist.append(data['likehood'][np.where(data['temp']>1e-4)[0][-1]])
            maxlist.append(data['likehood'][0])

        # Now do noise

        mod = "noise"

        colour = ncolor

        try:
            names = ['temp','likehood','error','acl']
            data = np.recfromtxt(str(jobName)+"{0}_evidence.dat".format(mod), names=names)
        except:
            try:
                names = ['temp','likehood','error']
                data = np.recfromtxt(str(jobName)+"{0}_evidence.dat".format(mod), names=names)
            except:
                print('No noise evidence')
        #error = np.zeros(likehood.shape)
        plt.semilogx(data['temp'], data['likehood'], label=mod, linewidth=2, color=colour)
        plt.errorbar(data['temp'], data['likehood'], 2*data['error'], color=colour)
        minlist.append(data['likehood'][np.where(data['temp']>1e-4)[0][-1]])
        maxlist.append(data['likehood'][0])


    plt.ylim(min(minlist)-1000,max(maxlist)+500)

    plt.xlabel( '1/Temp' )
    plt.ylabel('log(L)')
    plt.grid()
    plt.legend(loc=2)
    plt.savefig(plotsDir+'likelihood.png')
    plt.close()

def plot_likelihood_2(modelList, plotsDir, fullOnly_flag=0):
    plt.figure()
    plt.title('Likelihood')

    if fullOnly_flag:
        mod = 'full'
        try:
            names = ['temp','likehood','error','acl']
            data = np.recfromtxt(str(jobName)+"{0}_evidence.dat".format(mod), names=names)
        except:
            try:
                names = ['temp','likehood','error']
                data = np.recfromtxt(str(jobName)+"{0}_evidence.dat".format(mod), names=names)
            except:
                pass
        plt.semilogx(data['temp'], data['likehood']*data['temp'], label=mod, linewidth=2, color=colour)
        plt.errorbar(data['temp'], data['likehood']*data['temp'], 2*data['error'], color=colour)


    else:
        for mod in modelList:
            if mod == 'glitch':
                colour = gcolor
            elif mod == 'signal':
                colour = scolor
            else:
                colour = ncolor
            try:
                names = ['temp','likehood','error','acl']
                data = np.recfromtxt(str(jobName)+"{0}_evidence.dat".format(mod), names=names)
            except:
                try:
                   names = ['temp','likehood','error']
                   data = np.recfromtxt(str(jobName)+"{0}_evidence.dat".format(mod), names=names)
                except:
                    continue
            plt.semilogx(data['temp'], data['likehood']*data['temp'], label=mod, linewidth=2, color=colour)
            plt.errorbar(data['temp'], data['likehood']*data['temp'], 2*data['error'], color=colour)
    plt.grid()
    plt.xlabel('1/Temp')
    plt.ylabel('log(L) X 1/Temp')
    plt.legend(loc=2)
    plt.savefig(plotsDir+'TL.png')
    plt.close()

# -------------------------
# Plot dimensions of model
# -------------------------


def plot_model_dims(modelList, ifoList, ifoNames, plotsDir, fullOnly_flag=0):
    lineStyles = ['-', '--', ':','-','--',':']
    lineColors = ifoColors
    glitchChains = []
    signalChains = []
    # -- Read in data
    if fullOnly_flag==1:
        intChainFile = 'chains/'+str(jobName)+"full_model.dat.0"
        N = len(open(intChainFile).readlines())
        chains = np.loadtxt(intChainFile,skiprows=int(N/2))
        chains = np.transpose(chains)
        glitchChains = chains[3:3+len(ifoList)]
        signalChains = chains[2]
    
    else:
        for mod in modelList:
            intChainFile = 'chains/'+str(jobName)+"{0}_model.dat.0".format(mod)
            N = len(open(intChainFile).readlines())
            chains = np.loadtxt(intChainFile,skiprows=int(N/2))
            chains = np.transpose(chains)
            if mod == 'glitch':
                glitchChains = chains[3:3+len(ifoList)]
            elif mod == 'signal':
                signalChains = chains[2]

    signalOnly_flag = 0
    glitchOnly_flag = 0
    
    if len(signalChains) == 0:
        signalOnly_flag == 1
    if len(glitchChains) == 0:
        glitchOnly_flag == 1

    # -- Variation of the model dimension over MCMC iterations. 2 subplots for signal and glitch models

    if len(modelList) == 2:
        fig, (ax1, ax2) = plt.subplots(2, sharex=True, sharey=True)
        ax1.set_title('Model Dimension')
        if len(signalChains) > 0:
            ax1.plot(signalChains, linewidth=2, color=scolor, label='Signal')
        if len(glitchChains) > 0:
            for ifo in ifoList:
                ifoint=int(ifo)
                ax2.plot(glitchChains[ifoint], lineStyles[ifoint], color=lineColors[ifoint], linewidth=2, label=ifoNames[ifoint])

            # -- Legend placement outside the plot
            box = ax1.get_position()
            ax1.set_position([box.x0, box.y0, box.width*0.8, box.height])
            box = ax2.get_position()
            ax2.set_position([box.x0, box.y0, box.width*0.8, box.height])
            ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            # -- Make subplots close to each other and hide x ticks for all but bottom plot
            fig.subplots_adjust(hspace=0.1, right=0.8)
            plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
            plt.xlabel( 'MC step/10' )
            ax1.set_ylabel('Signal Model')
            ax1.grid()
            ax2.set_ylabel('Glitch Model')
            ax2.grid()
            plt.savefig(plotsDir+'model_dimensions.png')
            plt.close()
    
    elif len(modelList) == 1:
        fig = plt.figure()
        ax1 = plt.subplot(111)
        ax1.set_title('Model Dimension')
        if len(signalChains) > 0:
            ax1.plot(signalChains, linewidth=2, color=scolor, label='Signal')
        if len(glitchChains) > 0:
            for ifo in ifoList:
                ifoint=int(ifo)
                ax1.plot(glitchChains[ifoint], lineStyles[ifoint], color=lineColors[ifoint], linewidth=2, label=ifoNames[ifoint])
            
            # -- Legend placement outside the plot
            box = ax1.get_position()
            ax1.set_position([box.x0, box.y0, box.width*0.8, box.height])
            ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        # -- Make subplots close to each other and hide x ticks for all but bottom plot
        #plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
        plt.xlabel( 'MC step/10' )
        ax1.set_ylabel('{0} model'.format(modelList[0]))
        ax1.grid()
        plt.savefig(plotsDir+'model_dimensions.png')
        plt.close()
    
    
    
    if len(modelList) == 2:
        # -- Histogram of the model dimension. 2 subplots for signal and glitch models
        if len(signalChains) > 0 and len(glitchChains) > 0:
            fig, (ax1, ax2) = plt.subplots(2, sharex=True)
    else:
        fig, (ax1, ax2) = plt.subplots(2, sharex=True)
    ax1.set_title('Model Dimension Histogram')
    if len(signalChains) > 0:
        n,bins,patches = ax1.hist(signalChains, bins=np.arange(int(min(signalChains))-.5, int(max(signalChains))+1.5, 1), histtype='bar', color=scolor, log=False)
    if len(glitchChains) > 0:
        data = np.dstack(glitchChains)[0]
        n,bins,patches = ax2.hist(data, bins=np.arange(int(data.min())-.5, int(data.max())+1.5, 1), label=ifoNames, histtype='bar', log=False, color=[lineColors[int(i)] for i in ifoList])
        # -- Legend placement outside the plot
        box = ax1.get_position()
        ax1.set_position([box.x0, box.y0, box.width*0.8, box.height])
        box = ax2.get_position()
        ax2.set_position([box.x0, box.y0, box.width*0.8, box.height])
        ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        # -- Make subplots close to each other and hide x ticks for all but bottom plot
        fig.subplots_adjust(hspace=0.1, right=0.8)
        plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
        plt.xlabel('Model Dimension')
        ax1.set_ylabel('Signal Model')
        ax1.grid()
        ax2.set_ylabel('Glitch Model')
        ax2.grid()
        plt.savefig(plotsDir+'model_dimensions_histo.png')
    
    elif len(modelList) == 1:
        # -- Histogram of the model dimension. 2 subplots for signal and glitch models
        fig = plt.figure()
        ax1 = plt.subplot(111)
        ax1.set_title('Model Dimension Histogram')
        if len(signalChains) > 0:
            n,bins,patches = ax1.hist(signalChains, bins=np.arange(int(min(signalChains))-.5, int(max(signalChains))+1.5, 1), histtype='bar', alpha = 0.7, edgecolor='k', color=scolor, log=False)
        if len(glitchChains) > 0:
            data = np.dstack(glitchChains)[0]
            #ax2.set_prop_cycle(cycler('color',['darkgoldenrod','darkkhaki','dkarsage']))
            n,bins,patches = ax1.hist(data, bins=np.arange(int(data.min())-.5, int(data.max())+1.5, 1), label=ifoNames, histtype='bar', alpha=0.7, edgecolor='k', log=False, color=[lineColors[int(i)] for i in ifoList])
        # -- Legend placement outside the plot
        box = ax1.get_position()
        ax1.set_position([box.x0, box.y0, box.width*0.8, box.height])
        ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        # -- Make subplots close to each other and hide x ticks for all but bottom plot
        #plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
        plt.xlabel('Model Dimension')
        ax1.set_ylabel('{0} model'.format(modelList[0]))
        ax1.grid()
        plt.savefig(plotsDir+'model_dimensions_histo.png')

    # -- Calculate bayes factors for signal+glitch vs signal only, glitch only
    if fullOnly_flag:
        signal_on = signalChains > 0
        glitch_on = np.sum(glitchChains[0:len(ifoList)+1],axis=1)>0
        
        ev_sig = 0
        ev_glitch = 0
        ev_full = 0
        for s, g in zip(signal_on, glitch_on):
            if s and not g:
                ev_sig += 1
            if g and not s:
                ev_glitch += 1
            if s and g:
                ev_full += 1
    else:
        ev_sig = 0
        ev_glitch = 0
        ev_full = 0

    # Actually full vs. signal only
    try:
        sig_noise = ev_full/ev_sig
    except:
        sig_noise = np.inf

    # Actually full vs. glitch only
    try:
        sig_gl = ev_full/ev_gl
    except:
        sig_gl = np.inf

    return sig_noise, sig_gl



######################################################################################################################
#
# BWB webpage production
#
######################################################################################################################


def whitened_residual_plots(model,ifoList,ifoNames,fullOnly_flag=0):
    
    if model == 'glitch':
        lineColors = ifoColors
    elif model == 'signal':
        lineColors = signal_ifoColorList
    elif model == 'noise':
        colour = ncolor
        lineColors = ifoColors
    
    hashlist = ['solid','dashed','dashdot','solid','dashed','dashdot']

    plt.clf()

    # -- Plot N[0,1]
    mu = 0
    variance = 1
    sigma = math.sqrt(variance)
    x = np.linspace(-4, 4, 100)
    cdf = [1.0-0.5*(1.+math.erf(xx/math.sqrt(2.))) for xx in x]
    
    residual = {}
    whitened = {}
    psd_info = {}

    for ifo in ifoList:
      
        if fullOnly_flag==1:
            filename = 'post/full/{1}_median_PSD_{0}.dat'.format(ifoNames[int(ifo)],model)
        else:
            filename = 'post/{1}/{1}_median_PSD_{0}.dat'.format(ifoNames[int(ifo)],model)
        psd_info[ifo] = get_waveform(filename)
            
        imin = 0
        while psd_info[ifo][1][imin] >= 1.0:
            imin += 1

        if fullOnly_flag==1:
            filename = 'post/full/fourier_domain_{1}_median_residual_{0}.dat'.format(ifoNames[int(ifo)],model)
        else:
            filename = 'post/{1}/fourier_domain_{1}_median_residual_{0}.dat'.format(ifoNames[int(ifo)],model)
        residual[ifo] = np.genfromtxt(filename)
        
        whitened[ifo] = []
        for i in range(0,len(psd_info[ifo][1])):
            whitened[ifo].append(residual[ifo][i,1]/math.sqrt(psd_info[ifo][1][i]/2.))

    
    #### --- Histograms
    plt.plot(x,scs.norm.pdf(x, mu, sigma),color='k',lw=1.4,label='$\mathcal{N}[0,1]$')
    for ifo in ifoList:
        plt.hist(whitened[ifo][imin::],bins=50,normed=True,color=lineColors[int(ifo)],label=ifoNames[int(ifo)],histtype='step')

    plt.title(model)
    plt.yscale('log')
    plt.ylim(10e-4,2)
    plt.legend(loc=1)
    plt.grid(True)
    
    plt.savefig('plots/{model}_whitened_residual_histograms.png'.format(model=model))

    #plt.show()
    plt.clf()

    #### --- CDFs
    plt.plot(x,cdf,color='k',lw=1.4,label='$\mathcal{N}[0,1]$')
    for ifo in ifoList:
        
        plt.hist(whitened[ifo][imin::],bins=50,normed=True,color=lineColors[int(ifo)],label=ifoNames[int(ifo)],histtype='step',cumulative=-1)

    plt.title(model)
    plt.yscale('log')
    plt.ylim(10e-4,2)
    plt.legend(loc=1)
    plt.grid(True)

    plt.savefig('plots/{model}_whitened_residual_cdfs.png'.format(model=model))

    #plt.show()
    plt.clf()


def make_skymap_page(htmlDir, plotsDir):
    #if not os.path.exists(plotsDir):
    #if 'waveform' in page: 
    #    subpage = open(htmlDir+'waveform.html', 'w')
    #else:
    #    subpage = open(htmlDir+html_dict[page][0]+'.html', 'w')
    subpage = open(htmlDir+'/skymap.html', 'w')
    # -- Start of html subpage
    subpage.write('<html>\n')
    subpage.write('<head>\n')
    subpage.write('</head>\n')
    subpage.write('<body>\n')

    subpage.write('    <h2>Sky map</h2>\n')

    # Display the skymap
    plotsrc = './'+plotsDir+'skymap.png'
    if not os.path.exists(plotsrc):
        subpage.write(' <h3>Sky map generation failed.</h3>\n')
    subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=650></a><br/>\n')
    # Display all the megasky results: remember to run megasky first, then megaplot
    if os.path.exists(str(jobName)+'megasky_results.dat'):
        megasky_results = np.loadtxt(str(jobName)+'megasky_results.dat')
        pixarea, area50, area90, searcharea, injcontour = megasky_results 
        subpage.write('    The area per pixel is {0:.2f} sq. degrees.<br/>\n'.format(pixarea))
        subpage.write('    The area of the 50% region is {0:.2f} sq. degrees.<br/>\n'.format(area50))
        subpage.write('    The area of the 90% region is {0:.2f} sq. degrees.<br/>\n'.format(area90))
        subpage.write('    The searched area is {0:.2f} sq. degrees.<br/>\n'.format(searcharea))
        subpage.write('    The injection was found at the {0:.2f} percent area contour.<br/>\n'.format(injcontour))
    # Display the other megaksy plots, if present
    plotsrc = './'+plotsDir+'ra_acf.png'
    if os.path.exists(plotsrc):
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    plotsrc = './'+plotsDir+'ra_chain.png'
    if os.path.exists(plotsrc):
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')

    subpage.write('  </div>\n')
    # -- End of html nubpage
    subpage.write('</body>\n')
    subpage.write('</html>\n')
    subpage.close()

def splash_page(plotsDir, ifoNames, snrList, bayeswaverunfile, sig_gl, sig_noise, fullOnly_flag=0):
    html_string = ''
    if not fullOnly_flag:
        html_string += '    <a href="./'+plotsDir+'odds.png"><img src="./'+plotsDir+'odds.png" style="float: right;" width=600 alt="odds.png"></a>\n'
    else:
        html_string += '    <h3>This run was done with the --fullOnly flag, meaning glitch and signal were run simultaneously.</h3>\n'
    html_string += '    <h3>Detector Names: {0}</h3>\n'.format(', '.join(ifoNames))
    if mdc:
        html_string += '    <h3>Matched Filter SNRs of Injections</h3>\n'
        for ifo, snr in zip(ifoNames, snrList):
            html_string += '    {0} injected with SNR {1:.1f}<br/>\n'.format(ifo, snr)
    html_string += '    <h3>Log Info</h3>\n'
    html_string += '    <a href="./'+bayeswaverunfile+'">See full log file</a>\n'
    if not restrictModel:
        html_string +='    <h3>This is a {0} only run.</h3>\n'.format(restrictModel)
        html_string +='    There are no Bayes factors for this run since it\'s only a single model.\n'
    if not fullOnly_flag:
        html_string +='    <h3>Evidence for Signal</h3>\n'
        html_string +='    log(Evidence_signal / Evidence_glitch) = {0:.1f} &plusmn; {1:.1f} <br/>\n'.format(sig_gl,err_sig_gl)
        html_string +='    log(Evidence_signal / Evidence_noise) = {0:.1f} &plusmn; {1:.1f} <br/>\n'.format(sig_noise,err_sig_noise)
    else:
        html_string +='    <h3>Bayes factor for signal+glitch vs signal only:</h3>\n'
        html_string +='    {0} <br/>\n'.format(sig_noise)

        html_string +='    <h3>Bayes factor for signal+glitch vs glitch only:</h3>\n'
        html_string +='    {0} <br/>\n'.format(sig_gl)
    html_string +='  </p>\n'

    return html_string


def make_homepage(htmlDir, plotsDir, model, ifoNames, snrList, bayeswaverunfile, sig_gl, sig_noise, splash_page_text, fullOnly_flag=0):
    summary = open(htmlDir+'summary.html', 'w')
    # -- Start of html page with basic info about the run
    summary.write('<html>\n')
    summary.write('<head>\n')
    summary.write('</head>\n')
    summary.write('<body>\n')
    summary.write('  <p>\n')
    summary.write('\n')
    # -- Odds plot
    summary.write(splash_page_text)
    # -- End of html page with basic info about the run
    summary.write('</body>\n')
    summary.write('</html>\n')
    summary.close()



def make_index(htmlDir, plotsDir, model, gps, ifoList, ifoNames, snrList, bayeswaverunfile, sig_gl, sig_noise, splash_page_text, fullOnly_flag=0):
    index = open('index.html', 'w')
    # -- Start of the BWB webpage
    index.write('<!DOCTYPE html/css>\n')
    index.write('<html>\n')
    index.write('<head>\n')
    index.write('<link rel="stylesheet" type="text/css" href="./html/BWBweb.css">\n')
    index.write('<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--!>\n')
    index.write('<script src="./html/secure_ajax.js"></script>\n')
    index.write('<script src="./html/navigate.js"></script>\n')
    index.write('</head>\n')
    # -- Title
    index.write('<body>\n')
    index.write('<div class="container wrapper">\n')
    index.write('  <div id="top">\n')
    index.write('    <h1>BayesWave Output Page</h1>\n')
    index.write('       <p>Results for trigger at GPS {0}</p>\n'.format(gps))
    index.write('  </div>\n')
    # -- Navigation bar
    index.write('  <div class="wrapper">\n')
    index.write('    <div id="menubar">\n')
    index.write('      <ul id="menulist">\n')
    index.write('        <li class="menuitem" id="summary">Model selection\n')
    if not restrictModel == 'glitch':
        index.write('        <li class="menuitem" id="signal">Signal model\n')
    if not restrictModel == 'signal':
        index.write('        <li class="menuitem" id="glitch">Glitch model\n')
    if fullOnly_flag == 1:
        index.write('        <li class="menuitem" id="full">signal+glitch model\n')
    #index.write('        <li class="menuitem" id="snr">SNR\n')
    if not restrictModel == 'glitch':
        index.write('        <li class="menuitem" id="signalmoments">Signal waveform moments\n')
    if not restrictModel == 'signal':
        index.write('        <li class="menuitem" id="glitchmoments">Glitch waveform moments\n')
    #index.write('        <li class="menuitem" id="t_at_hmax">Time at Max Amplitude\n')
    index.write('        <li class="menuitem" id="skymap">Skymap\n')
    if mdc:
        index.write('        <li class="menuitem" id="overlap">Overlap\n')


    index.write('        <li class="menuitem" id="diagnostics">Diagnostics\n')
    index.write('      </ul>\n')
    index.write('    </div>\n')
    # -- Main part
    index.write('    <div id="main">\n')
    index.write('      <p>\n')
    index.write('\n')
    index.write(splash_page_text)
    index.write('    </div>\n')

    # -- End of the BWB webpage
    index.write('</body>\n')
    index.write('</html>\n')
    index.close()



def make_model_page(htmlDir, plotsDir, model, ifoList, ifoNames):
    subpage = open(htmlDir+model+'.html','w') # make html file

    # -- Start of html subpage
    subpage.write('<html>\n')
    subpage.write('<head>\n')
    subpage.write('</head>\n')
    subpage.write('<body>\n')
    
    # To Plot:
    #   - time domain waveform
    #   - power spectra
    #   - tf tracks
    #   - Q scan
    #   - (if applicable) injeciton Q scan
    #   - Residual Q scan

    subpage.write('     <h2>{0} model</h2>'.format(model))

    ######### Time domain waveform #######
    subpage.write('         <center><h3>Time Domain Waveforms</h3></center>\n')
    for ifo in ifoList:
        plotsrc = './{plotsDir}{model}_waveform_{ifo}.png'.format(model=model,plotsDir=plotsDir,ifo=ifoNames[int(ifo)])
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    # -- Toggle for full plots
    subpage.write('<br/>See full axis: <a id="displayFull'+model+'timedomain" href="javascript:toggle(\'divFull'+model+'timedomain\',\'displayFull'+model+'timedomain\');">show</a>\n')
    subpage.write('  <div id="divFull'+model+'timedomain" style="display: none">\n')
    for ifo in ifoList:
        plotsrc = './{plotsDir}{model}_waveform_{ifo}_full.png'.format(model=model,plotsDir=plotsDir,ifo=ifoNames[int(ifo)])
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    subpage.write("</div>\n")
    subpage.write('  <br/>\n')
    subpage.write('  <hr>\n')


    if not model=='full':

      ######### Frequency domain waveform #######
      subpage.write('         <center><h3>Power Spectra</h3></center>\n')
      for ifo in ifoList:
          plotsrc = './{plotsDir}{model}_frequence_domain_{ifo}.png'.format(model=model,plotsDir=plotsDir,ifo=ifoNames[int(ifo)])
          subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
      subpage.write('  <br/>\n')
      subpage.write('  <hr>\n')


      ######### f(t) tracks #######
      subpage.write('         <center><h3>Frequency vs. Time</h3></center>\n')
      for ifo in ifoList:
          plotsrc = './{plotsDir}{model}_tf_{ifo}.png'.format(model=model,plotsDir=plotsDir,ifo=ifoNames[int(ifo)])
          subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
      subpage.write('  <br/>\n')
      subpage.write('  <hr>\n')

    ######### Spectrogram #######
    subpage.write('         <center><h3>Spectrogram of median waveform</h3></center>\n')
    for ifo in ifoList:
        plotsrc = './'+plotsDir+'{0}_spectrogram_Q8_{1}.png'.format(model,ifoNames[int(ifo)])
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    # -- Toggle for full plots
    subpage.write('<br/>Other Q resolutions: <a id="displayFull'+model+'spect\'" href="javascript:toggle(\'divFull'+model+'spect\',\'displayFull'+model+'spect\');">show</a>\n')
    subpage.write('  <div id="divFull'+model+'spect'+'" style="display: none">\n')
    for ifo in ifoList:
        plotsrc = './'+plotsDir+'{0}_spectrogram_Q4_{1}.png'.format(model,ifoNames[int(ifo)])
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    for ifo in ifoList:
        plotsrc = './'+plotsDir+'{0}_spectrogram_Q16_{1}.png'.format(model,ifoNames[int(ifo)])
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    subpage.write("</div>\n")
    subpage.write('  <br/>\n')
    subpage.write('  <hr>\n')

    ######### Data #######
    subpage.write('         <center><h3>Spectrogram of Data</h3></center>\n')
    for ifo in ifoList:
        plotsrc = './'+plotsDir+'data_spectrogram_Q8_{0}.png'.format(ifoNames[int(ifo)])
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    # -- Toggle for full plots
    subpage.write('<br/>Other Q resolutions: <a id="displayFull'+model+'data\'" href="javascript:toggle(\'divFull'+model+'data\',\'displayFull'+model+'data\');">show</a>\n')
    subpage.write('  <div id="divFull'+model+'data'+'" style="display: none">\n')
    for ifo in ifoList:
        plotsrc = './'+plotsDir+'data_spectrogram_Q4_{0}.png'.format(ifoNames[int(ifo)])
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    for ifo in ifoList:
        plotsrc = './'+plotsDir+'data_spectrogram_Q16_{0}.png'.format(ifoNames[int(ifo)])
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    subpage.write("</div>\n")
    subpage.write('  <br/>\n')
    subpage.write('  <hr>\n')


  ######### Residuals #######
    subpage.write('         <center><h3>Spectrogram of Residuals</h3></center>\n')
    for ifo in ifoList:
        plotsrc = './'+plotsDir+'{0}_residual_spectrogram_Q8_{1}.png'.format(model,ifoNames[int(ifo)])
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    # -- Toggle for full plots
    subpage.write('<br/>Other Q resolutions: <a id="displayFull'+model+'res\'" href="javascript:toggle(\'divFull'+model+'res\',\'displayFull'+model+'res\');">show</a>\n')
    subpage.write('  <div id="divFull'+model+'res'+'" style="display: none">\n')
    for ifo in ifoList:
        plotsrc = './'+plotsDir+'{0}_residual_spectrogram_Q4_{1}.png'.format(model,ifoNames[int(ifo)])
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    for ifo in ifoList:
        plotsrc = './'+plotsDir+'{0}_residual_spectrogram_Q16_{1}.png'.format(model,ifoNames[int(ifo)])
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    subpage.write("</div>\n")
    subpage.write('  <br/>\n')
    subpage.write('  <hr>\n')
    subpage.write('</body>\n')
    subpage.write('</html>\n')
    subpage.close()



def make_moments_page(htmlDir, plotsDir, model, ifoList, ifoNames):

    subpage = open(htmlDir+model+'moments.html','w')

    # -- Start of html subpage
    subpage.write('<html>\n')
    subpage.write('<head>\n')
    subpage.write('</head>\n')
    subpage.write('<body>\n')


    for page in ['t0_rec', 'dur_rec', 'f0_rec', 'band_rec', 'snr']:
        subpage.write('  <br/>\n')
        subpage.write('  <center><h2>{0}</h2></center>'.format(html_dict[page][1]))
        
        for ifo in ifoList:
            plotsrc = './'+plotsDir+model+'_'+html_dict[page][2]+'_{0}.png'.format(ifoNames[int(ifo)])
            subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
        subpage.write('    </br>\n')

    subpage.write('</body>\n')
    subpage.write('</html>\n')
    subpage.close()


def make_diagnostics_page(htmlDir, plotsDir, model, ifoList, ifoNames, modelList):
    
    subpage = open(htmlDir+'diagnostics.html','w')
    subpage.write('<html>\n')
    subpage.write('<head>\n')
    subpage.write('</head>\n')
    subpage.write('<body>\n')
    
    for plot in ['likelihood', 'model_dimensions_histo', 'model_dimensions']:
        plotsrc = './'+plotsDir+plot+'.png'
        subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
        if plot == 'likelihood':
            subpage.write('     </br>')

    subpage.write('         <center><h3>PDFs and CDFs of whitened (fourier domain) residuals, compared to N[0,1]</h3></center>\n')
    for mod in modelList:
        plotsrc = './'+plotsDir+'{0}_whitened_residual_histograms.png'.format(mod)
        if not os.path.exists(plotsrc):
            continue
        subpage.write('      <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
        plotsrc = './'+plotsDir+'{0}_whitened_residual_cdfs.png'.format(mod)
        subpage.write('      <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    #-- noise
    plotsrc = './'+plotsDir+'noise_whitened_residual_histograms.png'
    if os.path.exists(plotsrc):
        subpage.write('      <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
        plotsrc = './'+plotsDir+'noise_whitened_residual_cdfs.png'
        subpage.write('      <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
    subpage.write('     </br>')
    subpage.write('</body>\n')
    subpage.write('</html>\n')
    subpage.close()

def make_overlap_page(htmlDir, plotsDir, modelList, ifoList, ifoNames):

    subpage = open(htmlDir+'overlap.html','w')
    subpage.write('<html>\n')
    subpage.write('<head>\n')
    subpage.write('</head>\n')
    subpage.write('<body>\n')

    for mod in modelList:
        
        subpage.write('<center><h2>Overlap Histogram between recovered {0} and injection</h2></center>'.format(mod))
        for ifo in ifoList:
            plotsrc = './'+plotsDir+mod+'_overlap_{0}.png'.format(ifoNames[int(ifo)])
            subpage.write('    <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
        
        plotsrc = './'+plotsDir+mod+'_network_overlap.png'
        
        subpage.write('<center><h2>Network Overlap Histogram between recovered {0} and injection</h2></center>'.format(mod))
        subpage.write(' <center>')
        subpage.write('     <a href="'+plotsrc+'"><img src="'+plotsrc+'" alt="'+plotsrc+'" width=500></a>\n')
        subpage.write(' </center>')
        subpage.write('<hr>')

    subpage.write('</body>\n')
    subpage.write('</html>\n')
    subpage.close()


def make_webpage(htmlDir, model, mdc, gps, ifoList, ifoNames, modelList, snrList, sig_gl, sig_noise, postprocesspath):
    # TODO: move in separate function
    # -- Find out the path to the BayesWave executable

    os.system('cp '+postprocesspath+'/BWBweb.css '+htmlDir+'.')
    os.system('cp '+postprocesspath+'/secure_ajax.js '+htmlDir+'.')
    os.system('cp '+postprocesspath+'/navigate.js '+htmlDir+'.')

    splash_page_text = splash_page(plotsDir, ifoNames, snrList, bayeswaverunfile, sig_gl, sig_noise, fullOnly_flag)

    # -- Write the index
    make_index(htmlDir, plotsDir, model, gps, ifoList, ifoNames, snrList, bayeswaverunfile, sig_gl, sig_noise, splash_page_text, fullOnly_flag)
    # -- Write summary page (works as a homepage)
    make_homepage(htmlDir, plotsDir, model, ifoNames, snrList, bayeswaverunfile, sig_gl, sig_noise, splash_page_text, fullOnly_flag)
    for mod in modelList:
        make_model_page(htmlDir, plotsDir, mod, ifoList, ifoNames)
        make_moments_page(htmlDir, plotsDir, mod, ifoList, ifoNames)
    if fullOnly_flag == 1:
        make_model_page(htmlDir, plotsDir, 'full', ifoList, ifoNames)
    make_diagnostics_page(htmlDir, plotsDir, model, ifoList, ifoNames,modelList)
    if mdc:
        make_overlap_page(htmlDir, plotsDir, modelList, ifoList, ifoNames)
    make_skymap_page(htmlDir, plotsDir)




######################################################################################################################
#
# Main
#
######################################################################################################################
# -- Parse command line arguments

print("Running version compatable with --fullOnly flag\n\n")

parser = argparse.ArgumentParser(description='Produce html page for a bayeswave run.')


# -- Get basic info on the run
jobName, restrictModel, mdc, injFlag, bayeswaverunfile, ifoList, ifoNames, gps, snrList, info = readbwb()

if not restrictModel == '':
    model = restrictModel  #TODO: think about the --noiseOnly case
    modelList = [restrictModel]
    restrictModel = modelList
else:
    model = 'signal'
    restrictModel = ('signal', 'glitch', 'noise')
print("The mdc status is: {0}\n".format(mdc))
if injFlag:
    print("The injection was performed via an xml table\n")

if len(restrictModel) == 1:
    restrictModel = restrictModel[0]


# -- Create directory that will contain all plots
if not os.path.exists(plotsDir):
    os.makedirs(plotsDir)
    
# -- Read in time vector
time = np.loadtxt(str(jobName)+postDir+'timesamp.dat')
freq = np.loadtxt(str(jobName)+postDir+'freqsamp.dat')

# -- Don't worry about printing the colored stuff any more, so set worc to whitened only
worc = 'whitened'

for mod in modelList: 
    print("\nAnalyzing the {0} model".format(mod))
    
    # -- Loop over interferometers to create plots
    for ifo in ifoList:
        
        print('Processing {0} data'.format(ifoNames[int(ifo)]))
        
        # --------------------
        # Make waveform plots
        # --------------------

        # -- Determine axes for waveform plots
        axwinner = get_axes(jobName, postDir, ifoList, model, time, fullOnly_flag)

        # -- Read in the reconstructed time domain waveform
        if fullOnly_flag:
            filename = str(jobName)+postDir+'full/{1}_median_time_domain_waveform_{0}.dat'.format(ifoNames[int(ifo)], mod)
        else:
            filename = str(jobName)+postDir+'{1}/{1}_median_time_domain_waveform_{0}.dat'.format(ifoNames[int(ifo)], mod)
        timesamp,median_waveform, high_50, low_50, high_90, low_90 = get_waveform(filename)

        # -- Plot the median waveform and injected waveform (if present)
        plot_waveform(jobName, postDir, ifo, plotsDir, worc, mdc, mod, axwinner, time, low_50, high_50, low_90, high_90, median_waveform)
        
        
        # -- Read in reconstructed frequency waveform
        if fullOnly_flag==1:
            filename = str(jobName)+postDir+'full/{1}_median_frequency_domain_waveform_spectrum_{0}.dat'.format(ifoNames[int(ifo)],mod)
        else:
            filename = str(jobName)+postDir+'{1}/{1}_median_frequency_domain_waveform_spectrum_{0}.dat'.format(ifoNames[int(ifo)],mod)
        powerspec_info = get_waveform(filename)
        
        if fullOnly_flag==1:
            filename = str(jobName)+postDir+'full/{1}_median_PSD_{0}.dat'.format(ifoNames[int(ifo)],mod)
        else:
            filename = str(jobName)+postDir+'{1}/{1}_median_PSD_{0}.dat'.format(ifoNames[int(ifo)],mod)
        psd_info = get_waveform(filename)
        
        f_axwinner = get_axes_fdom(jobName, postDir, ifoList, model, time)

        # -- Plot median spectra
        plot_full_spectro(jobName, postDir, ifo, plotsDir, worc, mdc, mod, f_axwinner, psd_info, powerspec_info, median_waveform)
        

        
        # ---- tf track
        if fullOnly_flag==1:
            filename = str(jobName)+postDir+'full/{1}_median_tf_{0}.dat'.format(ifoNames[int(ifo)],mod)
        else:
          filename = str(jobName)+postDir+'{1}/{1}_median_tf_{0}.dat'.format(ifoNames[int(ifo)],mod)
        freqsamp, median_powerspec, high_50, low_50, high_90, low_90 = get_waveform(filename)
        

        plot_tf_tracks(jobName, postDir, ifo, plotsDir, worc, mdc, mod, axwinner, f_axwinner, time, low_50, high_50, low_90, high_90, median_powerspec);
        
    
        #---------------------------------------
        # Qscan (could make this cleaner)
        #---------------------------------------
        climwinner4 = [1,1]
        climwinner8 = [1,1]
        climwinner16 = [1,1]
        
        Q_scan(mod,mod, 4, time, freq, ifo, axwinner, f_axwinner, fullOnly_flag=fullOnly_flag)
        Q_scan(mod,mod, 8, time, freq, ifo, axwinner, f_axwinner, fullOnly_flag=fullOnly_flag)
        Q_scan(mod,mod, 16, time, freq, ifo, axwinner, f_axwinner, fullOnly_flag=fullOnly_flag)
        

        climwinner4 = Q_scan('data','data', 4, time, freq, ifo, axwinner, f_axwinner)
        climwinner8 = Q_scan('data','data', 8, time, freq, ifo, axwinner, f_axwinner)
        climwinner16 = Q_scan('data','data', 16, time, freq, ifo, axwinner, f_axwinner)
        
        Q_scan(mod,'{0}_residual'.format(mod), 4, time, freq, ifo, axwinner, f_axwinner, climwinner4, fullOnly_flag)
        Q_scan(mod,'{0}_residual'.format(mod), 8, time, freq, ifo, axwinner, f_axwinner, climwinner8, fullOnly_flag)
        Q_scan(mod,'{0}_residual'.format(mod), 16, time, freq, ifo, axwinner, f_axwinner, climwinner16, fullOnly_flag)


        if mdc:
            Q_scan('.','injected', 4, time, freq, ifo, axwinner, f_axwinner)
            Q_scan('.','injected', 8, time, freq, ifo, axwinner, f_axwinner)
            Q_scan('.','injected', 16, time, freq, ifo, axwinner, f_axwinner)

        
        # ---------------------------------------
        # Make histograms of waveform moments
        # ---------------------------------------

        # -- Read moments file and open mode output file
        if fullOnly_flag==1:
            momentsfile = str(jobName)+postDir+'full/{1}_{2}_moments_{0}.dat'.format(ifoNames[int(ifo)], mod, worc)
        else:
            momentsfile = str(jobName)+postDir+'{1}/{1}_{2}_moments_{0}.dat'.format(ifoNames[int(ifo)], mod, worc)

        moments = np.recfromtxt(momentsfile, names=True)

        if mdc: 
            injmoments = np.recfromtxt(str(jobName)+postDir+'injection_{1}_moments_{0}.dat'.format(ifoNames[int(ifo)], worc), names=True)
        else: 
            injmoments = moments

        # -- Histogram of the overlaps: use both linear and log10 y-axis
        if mdc:
            # Single detector
            make_histogram(moments, 'overlap', mod, ifo, plotsDir)

            # Detector network
            if len(ifoList)>1:
                make_histogram(moments, 'network_overlap', mod, ifo, plotsDir)


        # -- Make histograms with linear and log10 y-axis
        # Histogram central time
        make_histogram(moments, 't0_rec', mod, ifo, plotsDir)
        # Histogram of duration
        make_histogram(moments, 'dur_rec', mod, ifo, plotsDir)
        # Histogram of central frequency
        make_histogram(moments, 'f0_rec', mod, ifo, plotsDir)
        # Histogram of bandwidth
        make_histogram(moments, 'band_rec', mod, ifo, plotsDir)
        # Histogram of snr
        make_histogram(moments,'snr', mod, ifo, plotsDir)


                        
# -- Special case of fullOnly flag

if fullOnly_flag==1:
    print("\nMaking combined plots for --fullOnly run...")
    
    mod = 'full'

    for ifo in ifoList:
        worc = 'whitened'
        
        # -- Get axes as maximum of signal/glitch axes
        # TODO: what *is* the best way to scale the axes?? Maybe take min of [signal_min,glitch_min] and max of [signal_max,glitch_max] ??

        # for now use signal axes scaling
        axwinner = get_axes(jobName, postDir, ifoList, 'signal', time, fullOnly_flag)


        # -- plot time domain waveforms
        filename = str(jobName)+postDir+'full/{1}_median_time_domain_waveform_{0}.dat'.format(ifoNames[int(ifo)], 'signal', worc)
        timesamp, signal_median_waveform, signal_high_50, signal_low_50, signal_high_90, signal_low_90 = get_waveform(filename)

        filename = str(jobName)+postDir+'full/{1}_median_time_domain_waveform_{0}.dat'.format(ifoNames[int(ifo)], 'glitch', worc)
        timesamp, glitch_median_waveform, glitch_high_50, glitch_low_50, glitch_high_90, glitch_low_90 = get_waveform(filename)

        plot_waveform(jobName, postDir, ifo, plotsDir, worc, mdc, 'full', axwinner, time, signal_low_50, signal_high_50, signal_low_90, signal_high_90, signal_median_waveform, glitch_low_50, glitch_high_50, glitch_low_90, glitch_high_90, glitch_median_waveform)


        # -- And now do the Q scans
        climwinner4 = [1,1]
        climwinner8 = [1,1]
        climwinner16 = [1,1]

        Q_scan(mod,mod, 4, time, freq, ifo, axwinner, f_axwinner, fullOnly_flag=fullOnly_flag)
        Q_scan(mod,mod, 8, time, freq, ifo, axwinner, f_axwinner, fullOnly_flag=fullOnly_flag)
        Q_scan(mod,mod, 16, time, freq, ifo, axwinner, f_axwinner, fullOnly_flag=fullOnly_flag)
        
        
        climwinner4 = Q_scan('data','data', 4, time, freq, ifo, axwinner, f_axwinner)
        climwinner8 = Q_scan('data','data', 8, time, freq, ifo, axwinner, f_axwinner)
        climwinner16 = Q_scan('data','data', 16, time, freq, ifo, axwinner, f_axwinner)
        
        Q_scan(mod,'{0}_residual'.format(mod), 4, time, freq, ifo, axwinner, f_axwinner, climwinner4, fullOnly_flag)
        Q_scan(mod,'{0}_residual'.format(mod), 8, time, freq, ifo, axwinner, f_axwinner, climwinner8, fullOnly_flag)
        Q_scan(mod,'{0}_residual'.format(mod), 16, time, freq, ifo, axwinner, f_axwinner, climwinner16, fullOnly_flag)


# -- Plot evidence
sig_gl, sig_noise, err_sig_gl, err_sig_noise = plot_evidence(jobName, plotsDir)

# -- Plot Temp Chains
# WARNING: This feature appears to be broken because of a new file format!

try:
    plot_likelihood_1(modelList, plotsDir, fullOnly_flag)
    plot_likelihood_2(modelList, plotsDir, fullOnly_flag)
except:
    print("Failed to plot temp vs. likelihood.  This may be due to a change in file format for files like signal_evidence.dat")

# -- Plot the evolution of the dimensions of each model; do it only if signal and/or glitch model are present


if 'signal' in modelList or 'glitch' in modelList:
    ev = plot_model_dims(modelList, ifoList, ifoNames, plotsDir, fullOnly_flag)
    if fullOnly_flag:
        sig_noise = ev[0]
        sig_gl = ev[1]


# -- Posterior predictive checks:
# TODO: add more posterior predictive checsk to output page
for mod in modelList:
    try:
        whitened_residual_plots(mod,ifoList,ifoNames,fullOnly_flag)
    except Exception as e:
        print("Error producing %s residual histograms"%mod)
#        print(traceback.format_exc())
try:
    whitened_residual_plots('noise',ifoList,ifoNames,fullOnly_flag)
except Exception as e:
    print("Error producing noise residual histograms")


# -- Create directory that will contain all webpage code
if not os.path.exists(htmlDir):
    os.makedirs(htmlDir)

# -- Make webpage
make_webpage(htmlDir, model, mdc, gps, ifoList, ifoNames, modelList, snrList, sig_gl, sig_noise, postprocesspath)

# Move back to original dir
os.chdir(topdir)

print("\nWebpage script ran to completion with the above exceptions")
